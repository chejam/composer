Первоисточник тут https://github.com/RobLoach/docker-composer.
Пришлось "сделать свое" потому что в первоисточнике не получилось собрать приложение.

Сборка приложения 
```
#!bash

docker run -it -v $(pwd):/app -v ~/.ssh:/root/.ssh chejam/composer install
```